


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
	<title>Document</title>
	
	<link href="{!! asset('CSS/app.css') !!}" media="all" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="{!! asset('javas/app.js') !!}"></script>



</head>
  <body>

<form method="POST" action="/Contact" auto-complete="off">

@if(count($errors))

	<div class="alert alert-danger">

		<strong>Whoops!</strong> There were some problems with your input.

		<br/>

		<ul>

			@foreach($errors->all() as $error)

			<li>{{ $error }}</li>

			@endforeach

		</ul>

	</div>

@endif

{{ csrf_field() }}
<input type="hidden" name="id" value="{{ csrf_token() }}">


<div class="row">
			<div class="col-md-6">
				<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
					<label for="name">Name:</label>
					<input type="text" id="name" name="name" class="form-control" placeholder="Enter Your Name" value="">
					<span class="text-danger">{{ $errors->first('name') }}</span>
				</div>
			</div>
            <div class="col-md-6">
				<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
					<label for="name">Address:</label>
					<input type="text" id="address" name="address" class="form-control" placeholder="Enter Your Address" value="">
					<span class="text-danger">{{ $errors->first('address') }}</span>
				</div>
			</div>
            <div class="col-md-6">
				<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
					<label for="name">Email:</label>
					<input type="email" id="email" name="email" class="form-control" placeholder="Enter Your Email" value="">
					<span class="text-danger">{{ $errors->first('email') }}</span>
				</div>
                <div class="form-group">
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
			</div>
            </form>
</body>
</html>