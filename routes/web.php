<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('/', function () {
    // return view('EditForm');
})->name('index');

*/
Route::get('editfromlink',function()
{
    return view('EditForm');
});
Route::get('/', 'ContactController@index')->name('index');
Route::get('EditForm/{id}','ContactController@EDIT')->name('edit');
Route::POST('Contact' , 'ContactController@SAVE')->name('store');


//Route::get('/', 'ContactController@formValidation');
//Route::post('form-validation', 'ContactController@formValidationPost');